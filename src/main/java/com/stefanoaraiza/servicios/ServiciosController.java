package com.stefanoaraiza.servicios;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apitechu/v2")  //Atención
public class ServiciosController {
    @GetMapping("/servicios")
    public List getServicios() {

        return ServiciosService.getAll();
    }

    @PostMapping("/servicios")
    public String addServicio(@RequestBody String newServicio) {
        try {
            ServiciosService.insertBatch(newServicio);
            return "OK";
        } catch (Exception ex) {
            return ex.getMessage();
        }
    }

    @GetMapping("servicios")
    public List getServiciosFilter(@RequestBody String filtro){
        return ServiciosService.getFiltrados(filtro);
    }

    @PutMapping("/servicios")
    public String updServicios(@RequestBody String data){
        try{
            JSONObject obj = new JSONObject(data);
            String filtro = obj.getJSONObject("filtro").toString();
            String updates = obj.getJSONObject("updates").toString();
            ServiciosService.update(filtro, updates);
            return "OK";
        }catch (Exception ex){
            return ex.getMessage();
        }
    }




}
